type ButtonLinkProps = {
    className?: string,
    href: string,
    target: string,
}

const ButtonLink: React.FC<ButtonLinkProps> = function ({ className = '', children, ...props }) {
    return (
        <a className={`flex justify-center bg-orange-400 border-b-4 border-orange-600 rounded font-title font-bold text-white tracking-widest uppercase tracking-wider px-4 py-2 ${className}`} { ...props }>
            { children }
        </a>
    )
}

export default ButtonLink