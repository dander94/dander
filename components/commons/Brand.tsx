import Image from 'next/image'

type Size = 'sm' | 'lg'

export default function Brand ({ size } : { size: Size }) {
    const imgSizes: Record<Size, number> = {
        'sm': 24,
        'lg': 36,
    }
    const textSizes: Record<Size, string> = {
        'sm': '',
        'lg': 'text-xl',
    }

    return (
        <div className="inline-flex items-center">
            <Image
                src="/images/logo.svg"
                height={imgSizes[size]}
                width={imgSizes[size]}
            />
            <span className={`font-title font-bold ${textSizes[size]} text-white ml-4`}>Alejandro Díaz</span>
        </div>
    )
}