type SkillProps = {
    textColor: string,
}

const Skill: React.FC<SkillProps> = function ({ textColor, children }) {
    return (
        <span className={`font-title font-bold ${textColor} `}>
            { children }
        </span>
    )   
}

export default Skill