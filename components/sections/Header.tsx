import Brand from "../commons/Brand"

export default function Header () {

    return (
        <div className="hidden md:flex md:justify-between md:items-center pt-4">
            <Brand size='sm'/>
            <div>
                <a className="text-white border-b border-solid border-white" href="https://www.linkedin.com/in/alejandro-diaz-perez/" target="_blank">LinkedIn</a>
                <span className="text-white mx-4">|</span>
                <a className="text-white border-b border-solid border-white" href="https://github.com/dander94" target="_blank">GitHub</a>
                <span className="text-white mx-4">|</span>
                <a className="text-white border-b border-solid border-white" href="https://gitlab.com/dander94" target="_blank">GitLab</a>
                <span className="text-white mx-4">|</span>
                <a className="text-white border-b border-solid border-white" href="mailto:dander.devel@gmail.com" target="_blank">Contact</a>
            </div>
        </div>
    )       
}