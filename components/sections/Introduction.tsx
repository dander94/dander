import Image from 'next/image'

import Skill from "../commons/Skill";

export default function Introduction () {
    const experienceYears = new Date().getFullYear() - 2016;

    return (
        <div className="lg:grid lg:grid-cols-2 lg:items-end mt-8 md:mt-[4rem] lg:mt-[6rem]">
            <div>
                <h3 className="font-title text-white text-4xl md:text-5xl mb-5">
                    Hi,
                </h3>
                <h1 className="font-title text-white font-bold text-6xl md:text-8xl mb-6">
                    I'm Dander
                </h1>
                <h2 className="font-title text-white text-3xl ">Full-Stack Developer</h2>
                <div className="shadow-lg bg-cultured rounded lg:max-w-[30rem] mt-10 py-5 px-6">
                    <p className="mb-5">
                        My name is Alejandro, but most people call me <strong>Dander</strong>.
                    </p>
                    <p className="mb-5">
                        I'm a senior full-stack developer with {experienceYears}+ years of experience. During this time, I've worked for 3 different 
                        companies and I've contributed to <strong>more than 10 projects</strong>.
                    </p>
                    <p className="mb-5">
                        I'm currently a Data Engineer at <a className="font-title font-bold text-primary underline" href="https://www.realbetisbalompie.es/" target="_blank">Real Betis Balompié</a>
                    </p>
                    <p>
                        My main skills are <Skill textColor="text-django"> Django</Skill>, <Skill textColor="text-react"> React</Skill>, 
                        and <Skill textColor="text-vue"> Vue</Skill>, using <Skill textColor="text-typescript"> Typescript</Skill> and
                        <Skill textColor="text-graphql"> GraphQL</Skill>.
                    </p>
                </div>
            </div>
            <div className="relative mt-8 lg:ml-10">
                <div className="hidden md:block absolute top-4 lg:top-[-6rem] right-4 lg:right-8 shadow-xl shadow-inner rounded bg-primary overflow-hidden w-[8rem] h-[8rem] lg:w-[12rem] lg:h-[12rem] p-8">
                    <Image
                        className="scale-x-[-1] crisp-edges"
                        src="/images/me.png"
                        layout='fill'
                    />
                </div>
                <div className="shadow-lg bg-cultured rounded mt-10 py-5 px-6">
                    <p className="mb-3">
                        <strong className="text-2xl"><span className="text-5xl text-primary mr-1">3</span> values</strong>
                    </p>
                    <p className="mb-5">
                        that I apply to every aspect of my life
                    </p>
                    <div className="flex border-l-8 border-solid border-primary mb-6">
                        <div className="text-left ml-4">
                            <p className="font-title font-bold text-xl text-primary">
                                Engage
                            </p>
                            <p className="mt-2">
                                It is important to get involved in an engaging culture that make you love what you are doing.
                            </p>
                        </div>
                    </div>
                    <div className="flex border-l-8 border-solid border-primary mb-6">
                        <div className="text-left ml-4">
                            <p className="font-title font-bold text-xl text-primary">
                                Evolve
                            </p>
                            <p className="mt-2">
                                I am constantly searching for opportunities to learn and improve. Personally and professionally, I strive to create a better version of myself.
                            </p>
                        </div>
                    </div>
                    <div className="flex border-l-8 border-solid border-primary mb-6">
                        <div className="text-left ml-4">
                            <p className="font-title font-bold text-xl text-primary">
                                Empathize
                            </p>
                            <p className="mt-2">
                                This is the most important one. Understand the client. Understand your company. Understand your coworkers.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}