import Image from 'next/image'

export default function Header () {

    return (
        <div className="pt-4 mt-10 mb-4">
            <p className="text-white text-center">
                “ He has a unique set of skills as an engineer but his most impressive skill is being empathetic with all stakeholders in a given project 
                in order to drive solutions.
            </p>
            <p className="text-white text-center mt-3 lg:mt-0">
                I am confident that any team Ale is on will enjoy working with him on a personal and professional level.  ”
            </p>
            <p className="text-white text-center mt-4">Adi Sundar - CTO at <a className="text-white underline" href="https://www.marblepay.com/" target="_blank"> Marble</a></p>
        </div>
    )       
}