import Image from 'next/image'
import Brand from '../commons/Brand';

import ButtonLink from "../commons/ButtonLink";

export default function MobileExtraFooter () {
    return (
        <div className="md:hidden bg-navy p-8">
            <div className="flex flex-wrap justify-between">
                <a className="text-white border-b border-solid border-white" href="https://www.linkedin.com/in/alejandro-diaz-perez/" target="_blank">LinkedIn</a>
                <span className="text-white">|</span>
                <a className="text-white border-b border-solid border-white" href="https://github.com/dander94" target="_blank">GitHub</a>
                <span className="text-white">|</span>
                <a className="text-white border-b border-solid border-white" href="https://gitlab.com/dander94" target="_blank">GitLab</a>
            </div>
            <ButtonLink className="mt-6" href="mailto:dander.devel@gmail.com" target="_blank">Contact</ButtonLink>
            <div className="relative shadow-xl shadow-inner rounded bg-primary w-full after:content-[''] after:block after:pb-[100%] mt-6">
                <Image
                    className="scale-x-[-1] crisp-edges"
                    src="/images/me.png"
                    layout='fill'
                />
            </div>
            <div className="text-center mt-4">
                <Brand size="lg"/>
            </div>
        </div>
    )
}