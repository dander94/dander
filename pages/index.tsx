import Head from 'next/head'
import Footer from '../components/sections/Footer'
import Header from '../components/sections/Header'
import Introduction from '../components/sections/Introduction'
import MobileExtraFooter from '../components/sections/MobileExtraFooter'


export default function Home() {
  return (
    <>
      <Head>
        <title>Dander - Full Stack Developer</title>
        <meta name="description" content="I'm Dander, a Full Stack Developer" />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
        <link rel="manifest" href="/site.webmanifest" />
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#2b3763" />
        <meta name="msapplication-TileColor" content="#2b5797" />
        <meta name="theme-color" content="#2b3763" />
        <link
          href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&family=Open+Sans:wght@400;700"
          rel="stylesheet"
        />
      </Head>

      <main>
        <div className="min-w-screen bg-gradient-to-b from-navy to-primary">
          <div className="flex flex-col container min-h-screen mx-auto px-4 md:px-10">
            <Header />
            <div className="flex flex-col justify-between flex-grow">
              <Introduction />
              <Footer />
            </div>
          </div>
        </div>
        <MobileExtraFooter />
      </main>
    </>
  )
}
