const { url } = require("inspector")

module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    colors: {
      white: '#fff',
      black: '#000',
      grey: '#555555',

      primary: '#6699FF',
      secondary:'#EAA079',
      navy: '#2B3763',
      orange: {
        600: '#DA5700',
        500: '#FF6600',
        400: '#F78533',
      },
      cultured: '#F2F7F8',

      django: '#092E20',
      graphql: '#e535ab',
      react: '#4499B0',
      typescript: '#2F74C0',
      vue: '#339F6F'
    },
    fontFamily: {
      text: '"Open Sans", sans-serif',
      title: '"Nunito", ui-serif',
    },
    extend: {
      backgroundImage: {
        'desktop': 'url("/images/doge.jpg")',
      },
    },
  },
  plugins: [],
}
